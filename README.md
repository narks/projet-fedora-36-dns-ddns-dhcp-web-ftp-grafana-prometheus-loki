# Projet Fedora 36 DNS-DDNS-DHCP-WEB-FTP-Grafana-Prometheus-Loki

Partie 1 : Introduction, différences entre la distribution RedHat et Debian, et comment installé une VM Fedora 36 sur VMWare.

Partie 2 : Procédure d'installation d'un serveur DNS, DDNS, DHCP (avec réservation et exclusions d'adresses), Web (Nginx) et le FTP (VSftp).

Partie 3 : Procédure de mise en place d'un serveur de supérvision (Grafana/Prometheus), d'un serveur d'analyse de Logs (Grafana/Loki), et une DMZ.
